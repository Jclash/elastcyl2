CC = mpic++
MPIINCLUDE=-I/usr/include/mpich
CFLAGS = $(MPIINCLUDE)
LCLAGS = $(CFLAGS)

.PHONY: plots, clean, clobber

main.o : main.cpp stdafx.h
	$(CC) $(CFLAGS) -c main.cpp

elastcyl2: main.o 
	$(CC) $(LCLAGS) main.o -o elastcyl2

solution.txt: elastcyl2
	@echo 
	@echo Running code...
	rm -f step*.txt
	#./elastcyl2
	mpirun -np 4 ./elastcyl2
	>solution.txt

plots.png: solution.txt plot_solution.py
	@echo 
	@echo Plotting results...
	rm -f plot*.png
	rm -f centerline*.png
	python plot_solution.py
	>plots.png

animate: plots.png
	@echo 
	@echo Animating results...
	#python animate_solution.py
	rm -f *.avi
	#./ffmpeg.exe -framerate 25 -r 3 -i plot_r%d.png solution_r.avi
	#./ffmpeg.exe -framerate 25 -i plot_I1%d.png solution_I1.avi
	#avconv -i plot_r%d.png solution_r.avi
	#avconv -i plot_r_mpi%d.png solution_r_mpi.avi
	#avconv -i plocm_I1%d.png solution_I1.avi
	#avconv -i plocm_sigma_e%d.png solution_sigma_e.avi
	#avconv -f image2 -i plotcm_I1%d.png -vcodec h264 -crf 1 -r 24 out.mov
	avconv -i plotcm_I1%d.png -s solution_I1.avi
	avconv -i plotcm_sigma_e%d.png solution_sigma_e.avi


clean:
	rm -f *.o elastcyl2

clobber: clean
	rm -f solution.txt *.png

resclean:
	rm -f *.png step*.txt *.avi solution.txt plots

allclean: clean resclean
	rm *~
	

