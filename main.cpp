// ConsoleApplication1.cpp : Defines the entry point for the console application.
//

#include <stdio.h>
#include <iostream>
#include <stdlib.h>
#include <fstream>
#include <string>

#define _USE_MATH_DEFINES
#include <math.h>

#include <sstream> // Required for stringstreams

//#include "time.h"

//#include <sys/time.h>
#include <ctime>
//#include <sys/resource.h>

#include "stdafx.h"

#ifdef GO_MPI == 1
#include <mpi.h>
#endif




/*
int N;
int M;

int N2;
int M2;

double grho;
double gksi;
*/

double *Ur0, *Ur1, *Ur2, *Ut0, *Ut1, *Ut2;

struct params{

	int N;
	int M;

	double alpha;
	double betta;
	double R;
	double epsilon;
	double dense;
	double mu;
	double lambda;

	double T;

	double tau;

	double *Ur0, *Ur1, *Ur2, *Ut0, *Ut1, *Ut2;

	double *r_i, *thetta_j;

	int sfreq;

	int N2;
	int M2;

	double grho;
	double gksi;

};

params a;

int init_params(params& par, int N = 200){

	par.N = N;
	par.M = N;

	par.sfreq = 5;

	par.alpha = M_PI / 4;
	par.betta = M_PI / 6;
	par.R = 1;
	par.epsilon = 0.1;
	par.dense = 1400.0;
	par.mu = 101.0;
	par.lambda = 86.0;//2.0 / 3.0*par.mu;

	par.T = 12.0;

	par.grho = (par.R - par.epsilon) / par.N;
	par.gksi = (2 * M_PI - 2 * par.alpha) / par.M;

	par.tau = par.grho / 10.0;

	//par.r_i = new double[par.N];
	//par.thetta_j = new double[par.M];

	return 0;
}

int print_params(params& par){
	using namespace std;

	cout << "N = " << par.N << endl;
	cout << "M = " << par.M << endl;
	cout << "rho = " << par.grho << endl;
	cout << "ksi = " << par.gksi << endl;
	cout << "tau = " << par.tau << endl;

	return 0;
}

double diffclock(double clock1, double clock2)
{
	//return ((clock2-clock1)*1000.0)/CLOCKS_PER_SEC;
	return ((clock2-clock1))/(double)CLOCKS_PER_SEC;
	//return clock2 - clock1;
}


double mclock(){
	return clock();
	//timeval tim;
	//gettimeofday(&tim, NULL);
	//return tim.tv_sec + (tim.tv_usec / 1000000.0);
}


std::string itos(int number){
	std::ostringstream oss;
	oss << number;
	return oss.str();
}

int save_vector(double* A, int N, const char* fname){
	using namespace std;
	ofstream out(fname, ios::out);

	string fname_str = fname;

	if (!out.fail()){
		for (int i = 0; i < N; i++) out << A[i] << endl;
		cout << "file " << fname_str << " was successfully created" << endl;
	}
	else{
		cout << "error creating output file" << endl;
	}

	out.close();

	return 0;
}

int save_grafic_2d_on01(double* A, int N, const char* fname){
	using namespace std;
	ofstream out(fname, ios::out);

	string fname_str = fname;

	if (!out.fail()){
		for (int i = 0; i < N; i++) out << (1.0*i) / (N - 1) << " " << A[i] << endl;
		cout << "file " << fname_str << " was successfully created" << endl;
	}
	else{
		cout << "error creating output file" << endl;
	}

	out.close();

	return 0;
}

int save_grafic_3d_onD(double* D, double* A, int N, const char* fname){
	using namespace std;
	ofstream out(fname, ios::out);

	string fname_str = fname;
	double val;
	double eps = 0.000000001;

	if (!out.fail()){
		for (int i = 0; i < N; i++){
			val = A[i];
			//if ((val < eps)&&(val > -eps)) val = 0.0;
			out << D[2 * i] << " " << D[2 * i + 1] << " " << val << endl;
		}
		//cout << "file " << fname_str << " was successfully created" << endl;
	}
	else{
		cout << "error creating output file" << endl;
	}

	out.close();

	return 0;
}



inline double r(int i){

	return a.epsilon + i*a.grho;

	/*
	if (i <= N2) return epsilon + i*grho*grho;
	if (i >= N-N2) return epsilon + grho*grho*N2 + grho*(N-2*N2) + (i-N-N2)*grho*grho;
	return epsilon + N2*grho*grho + (i-N2)*grho;
	*/
}

inline double thetta(int j){

	return a.alpha + j*a.gksi;

	/*
	if (j <= M2) return alpha + j*gksi*gksi;
	if (j >= M-M2) return alpha + gksi*gksi*M2 + gksi*(M-2*M2) + (j-M-M2)*gksi*gksi;
	return alpha + M2*gksi*gksi + (j-M2)*gksi;
	*/
}

inline int trunc_r(double p){

	for (int i = 0; i <= a.N; i++)
	if (r(i) > p) return i - 1;

	return a.N;
}

inline int trunc_thetta(double p){

	for (int i = 0; i <= a.M; i++)
	if (thetta(i) > p) return i - 1;

	return a.M;
}

inline double rho(int i){
	return r(i + 1) - r(i);
}

inline double ksi(int j){
	return thetta(j + 1) - thetta(j);
}

inline double aijrd(int i, int j){
	int M = a.M;
	return (Ur1[(i + 1)*(M + 1) + j] - Ur1[i*(M + 1) + j]) / rho(i);
}

inline double aijtd(int i, int j){
	int M = a.M;
	return (Ur1[(i)*(M + 1) + j + 1] - Ur1[i*(M + 1) + j]) / ksi(j);
}

inline double bijrd(int i, int j){
	int M = a.M;
	return (Ut1[(i + 1)*(M + 1) + j] - Ut1[i*(M + 1) + j]) / rho(i);
}

inline double bijtd(int i, int j){
	int M = a.M;
	return (Ut1[(i)*(M + 1) + j + 1] - Ut1[i*(M + 1) + j]) / ksi(j);
}

inline double aijrs(int i, int j){
	int M = a.M;
	return (Ur1[(i)*(M + 1) + j] - Ur1[(i - 1)*(M + 1) + j]) / rho(i - 1);
}

inline double aijts(int i, int j){
	int M = a.M;
	return (Ur1[(i)*(M + 1) + j] - Ur1[i*(M + 1) + j - 1]) / ksi(j - 1);
}

inline double bijrs(int i, int j){
	int M = a.M;
	return (Ut1[(i)*(M + 1) + j] - Ut1[(i - 1)*(M + 1) + j]) / rho(i - 1);
}

inline double bijts(int i, int j){
	int M = a.M;
	return (Ut1[(i)*(M + 1) + j] - Ut1[i*(M + 1) + j - 1]) / ksi(j - 1);
}

inline double Add(int i, int j){
	int M = a.M;
	double mu = a.mu;
	double lambda = a.lambda;
	return r(i)*(2 * mu + lambda)*aijrd(i, j) + lambda*bijtd(i, j) + lambda*Ur1[(i)*(M + 1) + j];
}

inline double Asd(int i, int j){
	int M = a.M;
	double mu = a.mu;
	double lambda = a.lambda;
	return r(i)*(2 * mu + lambda)*aijrs(i, j) + lambda*bijtd(i, j) + lambda*Ur1[(i)*(M + 1) + j];
}

inline double Ads(int i, int j){
	int M = a.M;
	double mu = a.mu;
	double lambda = a.lambda;
	return r(i)*(2 * mu + lambda)*aijrd(i, j) + lambda*bijts(i, j) + lambda*Ur1[(i)*(M + 1) + j];
}

inline double Ass(int i, int j){
	int M = a.M;
	double mu = a.mu;
	double lambda = a.lambda;
	return r(i)*(2 * mu + lambda)*aijrs(i, j) + lambda*bijts(i, j) + lambda*Ur1[(i)*(M + 1) + j];
}


inline double Bdd(int i, int j){
	int M = a.M;
	double mu = a.mu;
	return mu*(1.0 / r(i)*aijtd(i, j) + bijrd(i, j) - Ut1[(i)*(M + 1) + j] / r(i));
}

inline double Bds(int i, int j){
	int M = a.M;
	double mu = a.mu;
	return mu*(1.0 / r(i)*aijtd(i, j) + bijrs(i, j) - Ut1[(i)*(M + 1) + j] / r(i));
}

inline double Bsd(int i, int j){
	int M = a.M;
	double mu = a.mu;
	return mu*(1.0 / r(i)*aijts(i, j) + bijrd(i, j) - Ut1[(i)*(M + 1) + j] / r(i));
}

inline double Bss(int i, int j){
	int M = a.M;
	double mu = a.mu;
	return mu*(1.0 / r(i)*aijts(i, j) + bijrs(i, j) - Ut1[(i)*(M + 1) + j] / r(i));
}

inline double Cdd(int i, int j){
	int M = a.M;
	double mu = a.mu;
	double lambda = a.lambda;
	return (2 * mu + lambda) / r(i)*(bijtd(i, j) + Ur1[(i)*(M + 1) + j]) + lambda*aijrd(i, j);
}

inline double Cds(int i, int j){
	int M = a.M;
	double mu = a.mu;
	double lambda = a.lambda;
	return (2 * mu + lambda) / r(i)*(bijtd(i, j) + Ur1[(i)*(M + 1) + j]) + lambda*aijrs(i, j);
}

inline double Csd(int i, int j){
	int M = a.M;
	double mu = a.mu;
	double lambda = a.lambda;
	return (2 * mu + lambda) / r(i)*(bijts(i, j) + Ur1[(i)*(M + 1) + j]) + lambda*aijrd(i, j);
}

inline double Css(int i, int j){
	int M = a.M;
	double mu = a.mu;
	double lambda = a.lambda;
	return (2 * mu + lambda) / r(i)*(bijts(i, j) + Ur1[(i)*(M + 1) + j]) + lambda*aijrs(i, j);
}

inline double S(int i, int j){
	int M = a.M;
	int N = a.N;

	if ((i > 0) && (i<N) && (j>0) && (j < M))
		return (rho(i - 1) + rho(i))*(ksi(j - 1) + ksi(j)) / 4;
	else if ((i == 0) && (j == 0))
		return rho(0)*ksi(0) / 4;
	else if ((i == N) && (j == 0))
		return rho(N - 1)*ksi(0) / 4;
	else if ((i == N) && (j == M))
		return rho(N - 1)*ksi(M - 1) / 4;
	else if ((i == 0) && (j == M))
		return rho(0)*ksi(M - 1) / 4;
	else if (i == N)
		return rho(N - 1)*(ksi(j - 1) + ksi(j)) / 4;
	else if (i == 0)
		return rho(0)*(ksi(j - 1) + ksi(j)) / 4;
	else if (j == 0)
		return (rho(i - 1) + rho(i))*ksi(0) / 4;
	else if (j == M)
		return (rho(i - 1) + rho(i))*ksi(M - 1) / 4;

	return (rho(1 - 1) + rho(1))*(ksi(j - 1) + ksi(j)) / 4;

	std::cout << "strange situation: i = " << i << ", j = " << j << std::endl;

}

inline double S(int i, int j, int i1, int j1){
	//return ((rho(i1 - 1) + rho(i1))*(ksi(j1 - 1) + ksi(j1))) / ((rho(i - 1) + rho(i))*(ksi(j - 1) + ksi(j)));
	return S(i1, j1) / S(i, j);
}

inline double Fr(int i, int j){ return 0.0; }

inline double Ft(int i, int j){ return 0.0; }

inline double Sigma(int j, int k){
	//return 1.0;
	if (k <= 1){
		return 1.0;
	}
	else{
		return 0.0;
	}
}

int dF_dr(double* dF, double* F, params par){

	int N = par.N;
	int M = par.M;

	for (int i = 0; i <= N; i++){
		for (int j = 0; j <= M; j++){

			if ((i >= 1) && (i <= N - 1)){
				dF[i*(M + 1) + j] = (F[(i + 1)*(M + 1) + j] - F[(i - 1)*(M + 1) + j]) / (r(i + 1) - r(i - 1));
			}
			else if (i == 0){
				dF[i*(M + 1) + j] = (F[(i + 1)*(M + 1) + j] - F[(i)*(M + 1) + j]) / (r(1) - r(0));
			}
			else if (i == N){
				dF[i*(M + 1) + j] = (F[(i)*(M + 1) + j] - F[(i - 1)*(M + 1) + j]) / (r(N) - r(N - 1));
			}

		}
	}

	return 0;
}

int dF_dt(double* dF, double* F, params par){

	int N = par.N;
	int M = par.M;

	for (int i = 0; i <= N; i++){
		for (int j = 0; j <= M; j++){

			if ((j >= 1) && (j <= M - 1)){
				dF[i*(M + 1) + j] = (F[(i)*(M + 1) + (j + 1)] - F[(i)*(M + 1) + (j - 1)]) / (thetta(i + 1) - thetta(i - 1));
			}
			else if (j == 0){
				dF[i*(M + 1) + j] = (F[(i)*(M + 1) + j+1] - F[(i)*(M + 1) + j]) / (thetta(1) - thetta(0));
			}
			else if (j == M){
				dF[i*(M + 1) + j] = (F[(i)*(M + 1) + j] - F[(i)*(M + 1) + (j-1)]) / (thetta(M) - thetta(M - 1));
			}

		}
	}

	return 0;
}

double absd( double x){
  if (x > 0) return x;
  else return -x;
}

int elastcyl_2d(int mode){
	using namespace std;

	double *D, *Vr0, *Vt0;

	int myrank = 0, np = 1;

#if GO_MPI == 1
	MPI_Comm_rank(MPI_COMM_WORLD, &myrank);
	MPI_Comm_size(MPI_COMM_WORLD, &np);
#endif

	bool savesolution = false;
	bool printinfo = true;
	bool saveUrUt = false;
	bool saveTensors = false;
	bool saveInvariants = true;

    if( mode == EVERYDAY){
    	savesolution = true;
    	printinfo = true;
    }

	//params a;
	//init_params(a);
	if ( (myrank == 0) && printinfo ) print_params(a);

	double alpha = a.alpha;
	double betta = a.betta;
	double R = a.R;
	double epsilon = a.epsilon;
	double dense = a.dense;
	double mu = a.mu;
	double lambda = a.lambda;

	double T = a.T;

	int N = a.N;
	int M = a.M;

	double tau = a.tau;

	double dl1 = (M_PI - betta);
	double dl2 = (M_PI + betta);

	int sfreq = a.sfreq;

	//int l1 = (dl1 - grho*grho*N2)/grho + N2;
	//int l2 = (dl2 - grho*grho*N2)/grho + N2;

	//double l1 = (M_PI - betta - alpha) / gksi;
	//double l2 = (M_PI + betta - alpha) / gksi;

	int l1 = trunc_thetta(dl1);
	int l2 = trunc_thetta(dl2);

	if ((myrank == 0)&& printinfo ) cout << "l1 = " << l1 << endl;
	if ((myrank == 0)&& printinfo ) cout << "l2 = " << l2 << endl;

	D = new double[2 * (N + 1)*(M + 1)];
	Vr0 = new double[(N + 1)*(M + 1)];
	Vt0 = new double[(N + 1)*(M + 1)];
	Ur0 = new double[(N + 1)*(M + 1)];
	Ur1 = new double[(N + 1)*(M + 1)];
	Ur2 = new double[(N + 1)*(M + 1)];
	Ut0 = new double[(N + 1)*(M + 1)];
	Ut1 = new double[(N + 1)*(M + 1)];
	Ut2 = new double[(N + 1)*(M + 1)];

	double *Ur2test = new double[(N+1)*(M+1)];
	double *Ut2test = new double[(N+1)*(M+1)];


	double *Ur_dr = new double[(N + 1)*(M + 1)];
	double *Ur_dt = new double[(N + 1)*(M + 1)];
	double *Ut_dr = new double[(N + 1)*(M + 1)];
	double *Ut_dt = new double[(N + 1)*(M + 1)];
	double *Sigma_rr = new double[(N + 1)*(M + 1)];
	double *Sigma_tt = new double[(N + 1)*(M + 1)];
	double *Sigma_rt = new double[(N + 1)*(M + 1)];
	double *I1 = new double[(N + 1)*(M + 1)];
	double *Sigma_e = new double[(N + 1)*(M + 1)];





	// Domain
	for (int i = 0; i <= N; i++){
		for (int j = 0; j <= M; j++){
			D[(i*(M + 1) + j) * 2] = r(i);
			D[(i*(M + 1) + j) * 2 + 1] = thetta(j);
			//D[(i*(M + 1) + j) * 2] = i*grho + epsilon;
			//D[(i*(M + 1) + j) * 2 + 1] = j*gksi + alpha;
		}
	}


	// Initial conditions U0 = 0
	/*for (int i = 0; i < (M + 1)*(N + 1); i++){
	Ur0[i] = 0.0;
	Ut0[i] = 0.0;
	}*/
	for (int i = 0; i <= N; i++){
		for (int j = 0; j <= M; j++){
			Ur0[i*(M + 1) + j] = 0.0;// 0.001*cos(M_PI*(r(i) - epsilon) / (R - epsilon));
			Ut0[i*(M + 1) + j] = 0.0;
		}
	}


	// Initial conditions V0 = 0
	for (int i = 0; i < (M + 1)*(N + 1); i++){
		Vr0[i] = 0.0;
		Vt0[i] = 0.0;
	}


	/*	for (int i = 0; i < (M + 1)*(N + 1); i++){
	Ut1[i] = 0.0;
	Ur1[i] = 0.0;
	if (((i - (M + 1)*N) >= l1) && ((i - (M + 1)*N) <= l2))
	Ur1[i] = 0.0;
	else if (((i - (M + 1)*(N - 1)) >= l1) && ((i - (M + 1)*(N - 1)) <= l2))
	Ur1[i] = 0.0;
	else
	Ur1[i] = 0.0;
	}*/

	if ( ( myrank == 0 ) && savesolution ){
		save_grafic_3d_onD(D, Ur0, (M + 1)*(N + 1), "step0r.txt");
		save_grafic_3d_onD(D, Ut0, (M + 1)*(N + 1), "step0t.txt");


			  dF_dr(Ur_dr, Ur0, a);
			  dF_dt(Ur_dt, Ur0, a);
			  dF_dr(Ut_dr, Ut0, a);
			  dF_dt(Ut_dt, Ut0, a);

			  for (int i = 0; i < (N + 1)*(M + 1); i++)
				Sigma_rr[i] = (2 * mu + lambda)*Ur_dr[i] + lambda / r((int)(i*1.0 / (M + 1)))*(Ut_dt[i] + Ur0[i]);

			  for (int i = 0; i < (N + 1)*(M + 1); i++)
				Sigma_tt[i] = (2 * mu + lambda) / r((int)(i*1.0 / (M + 1)))*(Ut_dt[i] + Ur0[i]) + lambda*Ur_dr[i];

			  for (int i = 0; i < (N + 1)*(M + 1); i++)
				I1[i] = Sigma_rr[i] + Sigma_tt[i];

			  for (int i = 0; i < (N + 1)*(M + 1); i++)
				Sigma_e[i] = absd( Sigma_rr[i] - Sigma_tt[i] );


			  // saving Ur_dr
			  //string fname_str_r_dr = "step" + itos(k) + "r_dr.txt";
			  //save_grafic_3d_onD(D, Ur_dr, (M + 1)*(N + 1), fname_str_r_dr.c_str());
			  //if ((myrank == 0) && printinfo) cout << "file " << fname_str_r_dr << " was successfully created" << endl;

			  // saving Ur_dt
			  //string fname_str_r_dt = "step" + itos(k) + "r_dt.txt";
			  //save_grafic_3d_onD(D, Ur_dt, (M + 1)*(N + 1), fname_str_r_dt.c_str());
			  //if ((myrank == 0) && printinfo) cout << "file " << fname_str_r_dt << " was successfully created" << endl;

			  // saving Ut_dr
			  //string fname_str_t_dr = "step" + itos(k) + "t_dr.txt";
			  //save_grafic_3d_onD(D, Ut_dr, (M + 1)*(N + 1), fname_str_t_dr.c_str());
			  //if ((myrank == 0) && printinfo) cout << "file " << fname_str_t_dr << " was successfully created" << endl;

			  // saving Ut_dt
			  //string fname_str_t_dt = "step" + itos(k) + "t_dt.txt";
			  //save_grafic_3d_onD(D, Ut_dt, (M + 1)*(N + 1), fname_str_t_dt.c_str());
			  //if ((myrank == 0) && printinfo) cout << "file " << fname_str_t_dt << " was successfully created" << endl;

			  if( saveTensors ){
			    // saving Sigma_rr
                            string fname_str_sigma_rr = "step" + itos(0) + "sigma_rr.txt";
			    save_grafic_3d_onD(D, Sigma_rr, (M + 1)*(N + 1), fname_str_sigma_rr.c_str());
			    if ((myrank == 0) && printinfo) cout << "file " << fname_str_sigma_rr << " was successfully created" << endl;

			    // saving Sigma_tt
			    string fname_str_sigma_tt = "step" + itos(0) + "sigma_tt.txt";
			    save_grafic_3d_onD(D, Sigma_tt, (M + 1)*(N + 1), fname_str_sigma_tt.c_str());
			    if ((myrank == 0) && printinfo) cout << "file " << fname_str_sigma_tt << " was successfully created" << endl;
			  }


			  if( saveInvariants ){

				// saving I1
			    string fname_str_I1 = "step" + itos(0) + "I1.txt";
			    save_grafic_3d_onD(D, I1, (M + 1)*(N + 1), fname_str_I1.c_str());
			    if ((myrank == 0) && printinfo) cout << "file " << fname_str_I1 << " was successfully created" << endl;

			    // saving Sigma_e
			    string fname_str_Sigma_e = "step" + itos(0) + "sigma_e.txt";
			    save_grafic_3d_onD(D, Sigma_e, (M + 1)*(N + 1), fname_str_Sigma_e.c_str());
			    if ((myrank == 0) && printinfo) cout << "file " << fname_str_Sigma_e << " was successfully created" << endl;

			  }


	}

	//save_grafic_3d_onD(D, Ur1, (M + 1)*(N + 1), "step1r.txt");
	//save_grafic_3d_onD(D, Ut1, (M + 1)*(N + 1), "step1t.txt");

	for (int i = 0; i < (M + 1)*(N + 1); i++){
		Ur2[i] = 0.0;
		Ut2[i] = 0.0;
	}

	for (int i = 0; i < (M + 1)*(N + 1); i++){
		Ur1[i] = Ur0[i];
		Ut1[i] = Ut0[i];
	}


	// Main loop
	//	double ri, rip1, rim1, rim2;
	//	double thetta_j, thetta_jp1, thetta_jm1, thetta_jm2;
	//	double rhoi, rhoim1, rhoim2;
	//	double ksij, ksijm1, ksijm2;
	//	int p = i0;
	//	int l = i1;
	double timesumr, timesumt, timemul;
	int k = 1;
	double* tempU;
	double eps = 0.0;


	//	double aijrd, aijtd, bijrd, bijtd, aijrs, aijts, bijrs, bijts, aim1jrd, bim1jtd, aim1jtd, bim1jrd, bijm1td, aijm1rd, aijm1td, bijm1rd;
	//	double Addij, Addim1j, Bddij, Bddim1j, Bddijm1, Cddij, Cddijm1, Sijim1j, Sijijm1, Sij, Sijm1, Sim1j;
	//	double Frij, Ftij;

	int L = 1;
	int s = 1;
	int seq_iters_atstart = 2;
	int seq_iters_atfinish = T / tau - seq_iters_atstart;
	bool paral = false;

	int *rcs, *dls;
	int *recvcounts, *displs;	

	rcs = new int[np];
	dls = new int[np];

	recvcounts = new int[np];
	displs = new int[np];


	for( int i = 0; i < np; i++ )
	    dls[i] = N*( i*1.0 / np );	
	
	for( int i = 0; i < np - 1; i++ )
	    rcs[i] = dls[i+1] - dls[i];
	rcs[np-1] = N + 1 - dls[np-1];
	

	//int ib = N*(myrank*1.0 / np);
	int ib = dls[myrank];
	int ie = rcs[myrank] + dls[myrank] - 1;
	//int ie = N*((myrank + 1.0) / np);
	//if ( myrank > 0 ) ie--;

        for( int i = 0; i < np; i++ ){
	    displs[i] = dls[i]*(M+1);
	    recvcounts[i] = rcs[i]*(M+1);
	}	
	

	int i1, i2;
	int cs = s + 1;

	//cout << "myrank = " << myrank << " ib = " << ib << "; ie = " << ie << endl;
	//cout << "myrank = " << myrank << " recvcounts = " << recvcounts[myrank] << "; displs = " << displs[myrank] << endl;


	for (double t = 2 * tau; t < T; t += tau){

		if (k > seq_iters_atstart){

			cs--;
			if (cs == 0) cs = s;

			i1 = ib;
			if (myrank > 0)
				i1 -= (s - 1)*L;

			i2 = ie;
			if (myrank < np - 1)
				i2 += (s - 1)*L;

#if GO_MPI == 1
			if(np > 1) paral = true;
#endif

		}
		else{
			i1 = 0;
			i2 = N;
			paral = false;
		}

		for (int i = i1; i <= i2; i++){
			for (int j = 0; j <= M; j++){

				if (k == 1){
					timesumr = (tau*Vr0[i*(M + 1) + j]) + Ur1[i*(M + 1) + j];
					timesumt = (tau*Vt0[i*(M + 1) + j]) + Ut1[i*(M + 1) + j];
					timemul = tau*tau / 2;
				}
				else{
					timesumr = -Ur0[i*(M + 1) + j] + (2 * Ur1[i*(M + 1) + j]);
					timesumt = -Ut0[i*(M + 1) + j] + (2 * Ut1[i*(M + 1) + j]);
					timemul = tau*tau;
				}


				// Points N1
				if (((i >= 1) && (i <= N - 2)) && ((j >= 1) && (j <= M - 2))){

					Ur2[i*(M + 1) + j] = timesumr
						+ timemul*((
						+Add(i, j) / rho(i)
						- Add(i - 1, j)*S(i, j, i - 1, j) / rho(i - 1)
						+ Bdd(i, j) / ksi(j)
						- Bdd(i, j - 1)*S(i, j, i, j - 1) / ksi(j - 1)
						- Cdd(i, j)) / (r(i)*dense)
						+ Fr(i, j));

					Ut2[i*(M + 1) + j] = timesumt
						+ timemul*((
						+r(i)*Bdd(i, j) / rho(i)
						- r(i - 1)*Bdd(i - 1, j)*S(i, j, i - 1, j) / rho(i - 1)
						+ Cdd(i, j) / ksi(j)
						- Cdd(i, j - 1)*S(i, j, i, j - 1) / ksi(j - 1)
						+ Bdd(i, j)) / (r(i)*dense)
						+ Ft(i, j));

				}

				// Points N2, N13
				if ((i == N) && (((j >= 1) && (j <= l1 - 1)) || ((j >= l2 + 1) && (j <= M - 2)))){

					Ur2[i*(M + 1) + j] = timesumr
						+ timemul*((
						-Asd(N, j) / rho(N - 1)
						- Add(i - 1, j)*S(i, j, i - 1, j) / rho(i - 1)
						+ Bds(i, j) / ksi(j)
						- Bds(i, j - 1)*S(i, j, i, j - 1) / ksi(j - 1)
						- Cds(i, j)) / (r(i)*dense)
						+ Fr(i, j));

					Ut2[i*(M + 1) + j] = timesumt
						+ timemul*((
						-r(i)*Bds(i, j) / rho(N - 1)
						- r(i - 1)*Bdd(i - 1, j)*S(i, j, i - 1, j) / rho(i - 1)
						+ Cds(i, j) / ksi(j)
						- Cds(i, j - 1)*S(i, j, i, j - 1) / ksi(j - 1)
						+ Bds(i, j)) / (r(i)*dense)
						+ Ft(i, j));

				}

				// Points N3, N14
				if ((i == N - 1) && (j >= 1) && (j <= M - 2)){

					Ur2[i*(M + 1) + j] = timesumr
						+ timemul*((
						+ Add(i, j) / rho(i)
						- Add(N-2, j)*S(N-1, j, N-2, j) / rho(N - 2) 
						+ Asd(N, j)*S(N-1, j, N, j) / rho(N - 1)
						+ Bdd(N-1, j) / ksi(j)
						- Bdd(N-1, j - 1)*S(i, j, i, j - 1) / ksi(j - 1)
						- Cdd(N-1, j)) / (r(N-1)*dense)
						+ Fr(i, j));

					Ut2[i*(M + 1) + j] = timesumt
						+ timemul*((
						+ r(N-1)*Bdd(N-1, j) / rho(i)
						- r(N-2)*Bdd(N-2, j)*S(N-1, j, N-2, j) / rho(N-2)
						+ r(N)*Bds(N, j)*S(i, j, N, j) / rho(N - 1)
						+ Cdd(i, j) / ksi(j)
						- Cdd(N-1, j - 1)*S(i, j, i, j - 1) / ksi(j - 1)
						+ Bdd(i, j)) / (r(N-1)*dense)
						+ Ft(i, j));

				}

				// Points N4
				if ((i == 0) && (j >= 2) && (j <= M - 2)){

					Ur2[i*(M + 1) + j] = timesumr
						+ timemul*((
						+Add(i, j) / rho(i)
						+ Bdd(i, j) / ksi(j)
						- Bdd(0, j - 1)*S(i, j, i, j - 1) / ksi(j - 1)
						- Cdd(i, j)) / (r(i)*dense)
						+ Fr(i, j));

					Ut2[i*(M + 1) + j] = timesumt
						+ timemul*((
						+r(i)*Bdd(i, j) / rho(i)
						+ Cdd(i, j) / ksi(j)
						- Cdd(i, j - 1)*S(i, j, i, j - 1) / ksi(j - 1)
						+ Bdd(i, j)) / (r(i)*dense)
						+ Ft(i, j));
				}

				// Points N5
				if ((i >= 2) && (i <= N - 1) && (j == 0)){

					Ur2[i*(M + 1) + j] = timesumr
						+ timemul*((
						+Add(i, j) / rho(i)
						- Add(i - 1, j)*S(i, j, i - 1, j) / rho(i - 1)
						+ Bdd(i, j) / ksi(0)
						- Cdd(i, j)) / (r(i)*dense)
						+ Fr(i, j));

					Ut2[i*(M + 1) + j] = timesumt
						+ timemul*((
						+r(i)*Bdd(i, j) / rho(i)
						- r(i - 1)*Bdd(i - 1, j)*S(i, j, i - 1, j) / rho(i - 1)
						+ Cdd(i, j) / ksi(0)
						+ Bdd(i, j)) / (r(i)*dense)
						+ Ft(i, j));
				}

				// Points N6, N20
				if ((i >= 1) && (i <= N - 2) && (j == M)){

					Ur2[i*(M + 1) + j] = timesumr
						+ timemul*((
						+Ads(i, j) / rho(i)
						- Ads(i - 1, j)*S(i, j, i - 1, j) / rho(i - 1)
						- Bsd(i, j) / ksi(M - 1)
						- Bdd(i, j - 1)*S(i, j, i, j - 1) / ksi(j - 1)
						- Csd(i, j)) / (r(i)*dense)
						+ Fr(i, j));

					Ut2[i*(M + 1) + j] = timesumt
						+ timemul*((
						+r(i)*Bsd(i, j) / rho(i)
						- r(i - 1)*Bsd(i - 1, j)*S(i, j, i - 1, j) / rho(i - 1)
						- Csd(i, j) / ksi(M - 1)
						- Cdd(i, j - 1)*S(i, j, i, j - 1) / ksi(j - 1)
						+ Bsd(i, j)) / (r(i)*dense)
						+ Ft(i, j));

				}

				// Points N7, N21
				if ((i >= 1) && (i <= N - 2) && (j == M - 1)){

					Ur2[i*(M + 1) + j] = timesumr
						+ timemul*((
						+Add(i, j) / rho(i)
						- Add(i - 1, j)*S(i, j, i - 1, j) / rho(i - 1)
						+ Bdd(i, j) / ksi(M - 1)
						- Bdd(i, j - 1)*S(i, j, i, j - 1) / ksi(j - 1)
						+ Bsd(i, M)*S(i, j, i, M) / ksi(j)
						- Cdd(i, j)) / (r(i)*dense)
						+ Fr(i, j));

					Ut2[i*(M + 1) + j] = timesumt
						+ timemul*((
						+r(i)*Bdd(i, j) / rho(i)
						- r(i - 1)*Bdd(i - 1, j)*S(i, j, i - 1, j) / rho(i - 1)
						+ Cdd(i, j) / ksi(M - 1)
						- Cdd(i, j - 1)*S(i, j, i, j - 1) / ksi(j - 1)
						+ Csd(i, M)*S(i, j, i, j + 1) / ksi(j)
						+ Bdd(i, j)) / (r(i)*dense)
						+ Ft(i, j));

				}

				// Points N8
				if ((i == 0) && (j == 0)){

					Ur2[i*(M + 1) + j] = timesumr
						+ timemul*((
						+Add(i, j) / rho(i)
						+ Bdd(i, j) / ksi(0)
						- Cdd(i, j)) / (r(i)*dense)
						+ Fr(i, j));

					Ut2[i*(M + 1) + j] = timesumt
						+ timemul*((
						+r(i)*Bdd(i, j) / rho(i)
						+ Cdd(i, j) / ksi(0)
						+ Bdd(i, j)) / (r(i)*dense)
						+ Ft(i, j));

				}

				// Points N9
				if ((i == 1) && (j == 0)){

					Ur2[i*(M + 1) + j] = timesumr
						+ timemul*((
						+Add(i, j) / rho(i)
						- Add(i - 1, j)*S(i, j, i - 1, j) / rho(i - 1)
						+ Bdd(i, j) / ksi(0)
						- Cdd(i, j)) / (r(i)*dense)
						+ Fr(i, j));

					Ut2[i*(M + 1) + j] = timesumt
						+ timemul*((
						+r(i)*Bdd(i, j) / rho(i)
						- r(i - 1)*Bdd(i - 1, j)*S(i, j, i - 1, j) / rho(i - 1)
						+ Cdd(i, j) / ksi(0)
						+ Bdd(i, j)) / (r(i)*dense)
						+ Ft(i, j));

				}

				// Points N10
				if ((i == 0) && (j == 1)){

					Ur2[i*(M + 1) + j] = timesumr
						+ timemul*((
						+Add(i, j) / rho(i)
						+ Bdd(i, j) / ksi(1)
						- Bdd(i, j - 1)*S(i, j, 0, 0) / ksi(0)
						- Cdd(i, j)) / (r(i)*dense)
						+ Fr(i, j));

					Ut2[i*(M + 1) + j] = timesumt
						+ timemul*((
						+r(0)*Bdd(i, j) / rho(i)
						+ Cdd(i, j) / ksi(1)
						- Cdd(i, 0)*S(i, j, i, 0) / ksi(0)
						+ Bdd(i, j)) / (r(i)*dense)
						+ Ft(i, j));

				}

				// Points N11
				if ((i == 1) && (j == 1)){

					Ur2[i*(M + 1) + j] = timesumr
						+ timemul*((
						+Add(i, j) / rho(i)
						- Add(i - 1, j)*S(i, j, i - 1, j) / rho(i - 1)
						+ Bdd(i, j) / ksi(j)
						- Bdd(i, j - 1)*S(i, j, i, j - 1) / ksi(j - 1)
						- Cdd(i, j)) / (r(i)*dense)
						+ Fr(i, j));

					Ut2[i*(M + 1) + j] = timesumt
						+ timemul*((
						+r(i)*Bdd(i, j) / rho(i)
						- r(i - 1)*Bdd(i - 1, j)*S(i, j, i - 1, j) / rho(i - 1)
						+ Cdd(i, j) / ksi(j)
						- Cdd(i, j - 1)*S(i, j, i, j - 1) / ksi(j - 1)
						+ Bdd(i, j)) / (r(i)*dense)
						+ Ft(i, j));

				}

				// Points N12
				if ((i == N) && (j == 0)){

					Ur2[i*(M + 1) + j] = timesumr
						+ timemul*((
						- Asd(N, 0) / rho(N - 1)
						- Add(N - 1, 0)*S(i, 0, i - 1, j) / rho(i - 1)
						+ Bds(i, j) / ksi(j)
						- Cds(i, j)) / (r(i)*dense)
						+ Fr(i, j));

					Ut2[i*(M + 1) + j] = timesumt
						+ timemul*((
						- r(i)*Bds(i, j) / rho(i - 1)
						- r(i - 1)*Bdd(i - 1, j)*S(i, j, i - 1, j) / rho(i - 1)
						+ Cds(i, j) / ksi(j)
						+ Bds(i, j)) / (r(i)*dense)
						+ Ft(i, j));

				}

				// Points N15
				if ((i == N) && (j == M)){

					Ur2[i*(M + 1) + j] = timesumr
						+ timemul*((
						-Ass(i, j) / rho(i - 1)
						- Ads(i - 1, j)*S(i, j, i - 1, j) / rho(i - 1)
						- Bss(i, j) / ksi(j - 1)
						- Bds(i, j - 1)*S(i, j, i, j - 1) / ksi(j - 1)
						- Css(i, j)) / (r(i)*dense)
						+ Fr(i, j));

					Ut2[i*(M + 1) + j] = timesumt
						+ timemul*((
						-r(i)*Bss(i, j) / rho(i - 1)
						- r(i - 1)*Bsd(i - 1, j)*S(i, j, i - 1, j) / rho(i - 1)
						- Css(i, j) / ksi(j - 1)
						- Cds(i, j - 1)*S(i, j, i, j - 1) / ksi(j - 1)
						+ Bss(i, j)) / (r(i)*dense)
						+ Ft(i, j));

				}

				// Point N16
				if ((i == N - 1) && (j == M)){

					Ur2[i*(M + 1) + j] = timesumr
						+ timemul*((
						+Ads(i, j) / rho(i)
						- Ads(i - 1, j)*S(i, j, i - 1, j) / rho(i - 1)
						+ Ass(i + 1, j)*S(i, j, i + 1, j) / rho(i)
						- Bsd(i, j) / ksi(j - 1)
						- Bdd(i, j - 1)*S(i, j, i, j - 1) / ksi(j - 1)
						- Csd(i, j)) / (r(i)*dense)
						+ Fr(i, j));

					Ut2[i*(M + 1) + j] = timesumt
						+ timemul*((
						+r(i)*Bsd(i, j) / rho(i)
						- r(i - 1)*Bsd(i - 1, j)*S(i, j, i - 1, j) / rho(i - 1)
						+ Bss(i + 1, j)*S(i, j, i + 1, j) / rho(i)
						- Csd(i, j) / ksi(j - 1)
						- Cdd(i, j - 1)*S(i, j, i, j - 1) / ksi(j - 1)
						+ Bsd(i, j)) / (r(i)*dense)
						+ Ft(i, j));

				}

				// Points N17
				if ((i == N - 1) && (j == M - 1)){

					Ur2[i*(M + 1) + j] = timesumr
						+ timemul*((
						+Add(i, j) / rho(i)
						- Add(i - 1, j)*S(i, j, i - 1, j) / rho(i - 1)
						+ Asd(i + 1, j)*S(i, j, i + 1, j) / rho(i)
						+ Bdd(i, j) / ksi(j)
						- Bdd(i, j - 1)*S(i, j, i, j - 1) / ksi(j - 1)
						+ Bsd(i, j + 1)*S(i, j, i, j + 1) / ksi(j)
						- Cdd(i, j)) / (r(i)*dense)
						+ Fr(i, j));

					Ut2[i*(M + 1) + j] = timesumt
						+ timemul*((
						+r(i)*Bdd(i, j) / rho(i)
						- r(i - 1)*Bdd(i - 1, j)*S(i, j, i - 1, j) / rho(i - 1)
						+ Bds(i + 1, j)*S(i, j, i + 1, j) / rho(i)
						+ Cdd(i, j) / ksi(j)
						- Cdd(i, j - 1)*S(i, j, i, j - 1) / ksi(j - 1)
						+ Csd(i, j + 1)*S(i, j, i, j + 1) / ksi(j)
						+ Bdd(i, j)) / (r(i)*dense)
						+ Ft(i, j));

				}

				// Points N18
				if ((i == 0) && (j == M)){

					Ur2[i*(M + 1) + j] = timesumr
						+ timemul*((
						+Ads(i, j) / rho(i)
						- Bsd(i, j) / ksi(j - 1)
						- Bdd(i, j - 1)*S(i, j, i, j - 1) / ksi(j - 1)
						- Csd(i, j)) / (r(i)*dense)
						+ Fr(i, j));

					Ut2[i*(M + 1) + j] = timesumt
						+ timemul*((
						+r(i)*Bsd(i, j) / rho(i)
						- Csd(i, j) / ksi(j - 1)
						- Cdd(i, j - 1)*S(i, j, i, j - 1) / ksi(j - 1)
						+ Bsd(i, j)) / (r(i)*dense)
						+ Ft(i, j));

				}

				// Points N19
				if ((i == 0) && (j == M - 1)){

					Ur2[i*(M + 1) + j] = timesumr
						+ timemul*((
						+Add(i, j) / rho(i)
						+ Bdd(i, j) / ksi(j)
						- Bdd(i, j - 1)*S(i, j, i, j - 1) / ksi(j - 1)
						+ Bsd(i, j + 1)*S(i, j, i, j + 1) / ksi(j)
						- Cdd(i, j)) / (r(i)*dense)
						+ Fr(i, j));

					Ut2[i*(M + 1) + j] = timesumt
						+ timemul*((
						+r(i)*Bdd(i, j) / rho(i)
						+ Cdd(i, j) / ksi(j)
						- Cdd(i, j - 1)*S(i, j, i, j - 1) / ksi(j - 1)
						+ Csd(i, j + 1)*S(i, j, i, j + 1) / ksi(j)
						+ Bdd(i, j)) / (r(i)*dense)
						+ Ft(i, j));

				}

				// Points N22 - with a force on the boundary
				if ((i == N) && (j >= l1) && (j <= l2)){

					Ur2[i*(M + 1) + j] = timesumr
						+ timemul*((
						- Asd(N, j) / rho(N - 1)
						- Add(N - 1, j)*S(N, j, N - 1, j) / rho(N-1)
						+ Bds(N, j) / ksi(j)
						- Bds(N, j - 1)*S(N, j, N, j - 1) / ksi(j - 1)
						- Cds(N, j)) / (r(N)*dense)
						+ Sigma(j, k)*(ksi(j) + ksi(j - 1)) / (2 * S(N, j)*dense)
						+ Fr(N, j));

					Ut2[i*(M + 1) + j] = timesumt
						+ timemul*((
						- r(N)*Bds(N, j) / rho(N - 1)
						- r(N - 1)*Bdd(N - 1, j)*S(N, j, N - 1, j) / rho(N - 1)
						+ Cds(N, j) / ksi(j)
						- Cds(N, j - 1)*S(N, j, N, j - 1) / ksi(j - 1)
						+ Bds(N, j)) / (r(N)*dense)
						+ Ft(N, j));

					// Points N23
					if ((i == N) && (j == M - 1)){

						Ur2[i*(M + 1) + j] = timesumr
							+ timemul*((
							- Asd(N, j) / rho(N - 1)
							- Add(N - 1, j)*S(N, j, N - 1, j) / rho(N - 1)
							+ Bds(N, j) / ksi(j)
							- Bds(N, j - 1)*S(N, j, N, j - 1) / ksi(j - 1)
							+ Bss(N, j + 1)*S(N, j, N, j + 1) / ksi(j)
							- Cds(N, j)) / (r(N)*dense)
							+ Fr(N, j));

						Ut2[i*(M + 1) + j] = timesumt
							+ timemul*((
							-r(N - 1)*Bdd(N - 1, j)*S(N, j, N - 1, j) / rho(N - 1)
							- r(N)*Bds(N, j) / rho(N - 1)
							- Cds(N, j - 1)*S(N, j, N, j - 1) / ksi(j - 1)
							+ Cds(N, j) / ksi(j)
							+ Css(N, j + 1)*S(N, j, N, j + 1) / ksi(j)
							+ Bds(N, j)) / (r(N)*dense)
							+ Ft(N, j));

					}
				}
			}
		}

		//cout << "thettat at 10142: " << Ut2[10142] << endl;

		/*
		for (int i = 0; i < (M + 1)*(N + 1); i++){
		if ((Ur2[i] > -eps) && (Ur2[i] < eps) || (isnan(Ur2[i]) == true)) Ur2[i] = 0.0;
		if ((Ut2[i] > -eps) && (Ut2[i] < eps) || (isnan(Ut2[i]) == true)) Ut2[i] = 0.0;
		}
		*/

		//double tempUr00 = Ur2[0];
		//Ur2[0] = 0.1;

		//cout << "k = " << k << endl;

		int sfreq_loc = sfreq;

		//if ((t>0.6-epsilon-5*tau-rho(1))&&(t<0.6-epsilon+5*tau))){
		//if ( (t>0.55) && (t<0.65) || (t < 0.05)){
		//	sfreq_loc = sfreq*10;
		//}

		if ( savesolution &&  (((int)(t / tau)) % ((int)(T / tau / (sfreq_loc * T))) == 0) ){

			if( myrank == 0 ) cout << (int)(t / T * 100 + 1) << "%  t = " << t << endl;

			// Saving solution to files
#if GO_MPI == 1

			  MPI_Gatherv( Ur2+displs[myrank], recvcounts[myrank], MPI_DOUBLE, Ur2test, recvcounts, displs, MPI_DOUBLE, 0, MPI_COMM_WORLD );
			  if( ( myrank == 0 ) && saveUrUt ){
			    string fname_str_r_mpi = "step" + itos(k) + "r_mpi.txt";
			    save_grafic_3d_onD(D, Ur2test, (M + 1)*(N + 1), fname_str_r_mpi.c_str());
			    if( printinfo ) cout << "file " << fname_str_r_mpi << " was successfully created" << endl;
			  }

			  MPI_Gatherv( Ut2+displs[myrank], recvcounts[myrank], MPI_DOUBLE, Ut2test, recvcounts, displs, MPI_DOUBLE, 0, MPI_COMM_WORLD );
			  if( ( myrank == 0 ) && saveUrUt ){
			    string fname_str_t_mpi = "step" + itos(k) + "t_mpi.txt";
			    save_grafic_3d_onD(D, Ut2test, (M + 1)*(N + 1), fname_str_t_mpi.c_str());
			    if( printinfo ) cout << "file " << fname_str_t_mpi << " was successfully created" << endl;
			  }

#else
			if (saveUrUt){

				if ((myrank == 0) && saveUrUt){
					string fname_str_r = "step" + itos(k) + "r.txt";
					save_grafic_3d_onD(D, Ur2, (M + 1)*(N + 1), fname_str_r.c_str());
					if (printinfo) cout << "file " << fname_str_r << " was successfully created" << endl;
				}


				if ((myrank == 0) && saveUrUt){
					string fname_str_t = "step" + itos(k) + "t.txt";
					save_grafic_3d_onD(D, Ut2, (M + 1)*(N + 1), fname_str_t.c_str());
					if (printinfo) cout << "file " << fname_str_t << " was successfully created" << endl;
				}
			}

#endif

			
			if( myrank == 0 ){
#if GO_MPI == 0
			Ur2test = Ur2; // Only without MPI
			Ut2test = Ut2; // Only without MPI
#endif		

			  dF_dr(Ur_dr, Ur2test, a);
			  dF_dt(Ur_dt, Ur2test, a);
			  dF_dr(Ut_dr, Ut2test, a);
			  dF_dt(Ut_dt, Ut2test, a);

			  for (int i = 0; i < (N + 1)*(M + 1); i++)
				Sigma_rr[i] = (2 * mu + lambda)*Ur_dr[i] + lambda / r((int)(i*1.0 / (M + 1)))*(Ut_dt[i] + Ur2test[i]);

			  for (int i = 0; i < (N + 1)*(M + 1); i++)
				Sigma_tt[i] = (2 * mu + lambda) / r((int)(i*1.0 / (M + 1)))*(Ut_dt[i] + Ur2test[i]) + lambda*Ur_dr[i];

			  for (int i = 0; i < (N + 1)*(M + 1); i++)
				I1[i] = Sigma_rr[i] + Sigma_tt[i];

			  for (int i = 0; i < (N + 1)*(M + 1); i++)
				Sigma_e[i] = absd( Sigma_rr[i] - Sigma_tt[i] );


			  // saving Ur_dr
			  //string fname_str_r_dr = "step" + itos(k) + "r_dr.txt";
			  //save_grafic_3d_onD(D, Ur_dr, (M + 1)*(N + 1), fname_str_r_dr.c_str());
			  //if ((myrank == 0) && printinfo) cout << "file " << fname_str_r_dr << " was successfully created" << endl;

			  // saving Ur_dt
			  //string fname_str_r_dt = "step" + itos(k) + "r_dt.txt";
			  //save_grafic_3d_onD(D, Ur_dt, (M + 1)*(N + 1), fname_str_r_dt.c_str());
			  //if ((myrank == 0) && printinfo) cout << "file " << fname_str_r_dt << " was successfully created" << endl;

			  // saving Ut_dr
			  //string fname_str_t_dr = "step" + itos(k) + "t_dr.txt";
			  //save_grafic_3d_onD(D, Ut_dr, (M + 1)*(N + 1), fname_str_t_dr.c_str());
			  //if ((myrank == 0) && printinfo) cout << "file " << fname_str_t_dr << " was successfully created" << endl;

			  // saving Ut_dt
			  //string fname_str_t_dt = "step" + itos(k) + "t_dt.txt";
			  //save_grafic_3d_onD(D, Ut_dt, (M + 1)*(N + 1), fname_str_t_dt.c_str());
			  //if ((myrank == 0) && printinfo) cout << "file " << fname_str_t_dt << " was successfully created" << endl;

			  if( saveTensors ){
			    // saving Sigma_rr
                string fname_str_sigma_rr = "step" + itos(k) + "sigma_rr.txt";
			    save_grafic_3d_onD(D, Sigma_rr, (M + 1)*(N + 1), fname_str_sigma_rr.c_str());
			    if ((myrank == 0) && printinfo) cout << "file " << fname_str_sigma_rr << " was successfully created" << endl;

			    // saving Sigma_tt
			    string fname_str_sigma_tt = "step" + itos(k) + "sigma_tt.txt";
			    save_grafic_3d_onD(D, Sigma_tt, (M + 1)*(N + 1), fname_str_sigma_tt.c_str());
			    if ((myrank == 0) && printinfo) cout << "file " << fname_str_sigma_tt << " was successfully created" << endl;
			  }

			  double minv = a.grho*a.grho*a.grho;
			  for(int i = 0; i < N*M; i++){
			    if ( (absd(Ur2test[i]) < minv) || (absd(Ut2test[i]) < minv) ){
				  //I1[i] = 0.0;
				  //Sigma_e[i] = 0.0;
				}
			  }

			  if( saveInvariants ){

				// saving I1
			    string fname_str_I1 = "step" + itos(k) + "I1.txt";
			    save_grafic_3d_onD(D, I1, (M + 1)*(N + 1), fname_str_I1.c_str());
			    if ((myrank == 0) && printinfo) cout << "file " << fname_str_I1 << " was successfully created" << endl;

			    // saving Sigma_e
			    string fname_str_Sigma_e = "step" + itos(k) + "sigma_e.txt";
			    save_grafic_3d_onD(D, Sigma_e, (M + 1)*(N + 1), fname_str_Sigma_e.c_str());
			    if ((myrank == 0) && printinfo) cout << "file " << fname_str_Sigma_e << " was successfully created" << endl;

			  }

		    }

		}

		//Ur2[0] = tempUr00;


		tempU = Ur0;
		Ur0 = Ur1;
		Ur1 = Ur2;
		Ur2 = tempU;

		tempU = Ut0;
		Ut0 = Ut1;
		Ut1 = Ut2;
		Ut2 = tempU;


		// DATA EXCHANGE
		//
#if GO_MPI == 1
		if (paral && (cs == 1)){

			int count = L*s*(M+1);
			MPI_Status status;

			// I
			if( myrank > 0 ){ // (1)
				MPI_Send(Ur1 + ib*(M+1), count, MPI_DOUBLE, myrank-1, 1, MPI_COMM_WORLD);
				MPI_Send(Ut1 + ib*(M+1), count, MPI_DOUBLE, myrank-1, 1, MPI_COMM_WORLD);
			}

			if( myrank < np - 1 ){ // (2)
				MPI_Recv(Ur1 + (ie+1)*(M+1), count, MPI_DOUBLE, myrank+1, 1, MPI_COMM_WORLD, &status);
				MPI_Recv(Ut1 + (ie+1)*(M+1), count, MPI_DOUBLE, myrank+1, 1, MPI_COMM_WORLD, &status);
			}

			// II
			if( myrank < np - 1 ){ // (3)
				MPI_Send(Ur1 + (ie-s*L+1)*(M+1), count, MPI_DOUBLE, myrank+1, 2, MPI_COMM_WORLD);
				MPI_Send(Ut1 + (ie-s*L+1)*(M+1), count, MPI_DOUBLE, myrank+1, 2, MPI_COMM_WORLD);
			}

			if( myrank > 0 ){ // (4)
				MPI_Recv(Ur1 + (ib-s*L)*(M+1), count, MPI_DOUBLE, myrank-1, 2, MPI_COMM_WORLD, &status);
				MPI_Recv(Ut1 + (ib-s*L)*(M+1), count, MPI_DOUBLE, myrank-1, 2, MPI_COMM_WORLD, &status);
			}

		MPI_Barrier(MPI_COMM_WORLD);

		}
#endif



		k++;
	}

	//save_grafic_3d_onD(D, U1, (M + 1)*(N + 1), "stepLast.txt");

	delete[](D);
	delete[](Vr0);
	delete[](Vt0);
	delete[](Ur0);
	delete[](Ur1);
	delete[](Ur2);
	delete[](Ut0);
	delete[](Ut1);
	delete[](Ut2);

	return 0;

}

int mpi_estimate(){
  using namespace std;

int myrank = 0, np = 1;

#if GO_MPI == 1
	MPI_Comm_rank(MPI_COMM_WORLD, &myrank);
	MPI_Comm_size(MPI_COMM_WORLD, &np);
#endif

  double aclock1, aclock2;

  for( int i = 100; i <= 1000; i = i + 100){

    init_params( a, i);

    aclock1 = mclock();

    elastcyl_2d( MPIESTIMATE );

    aclock2 = mclock();
    if (myrank == 0) cout << "Total time: " << diffclock(aclock1, aclock2) << "s" << endl << endl;

  }

  return 0;

}

int main(int argc, char* argv[])
{
	using namespace std;

	int myrank = 0, np = 1;

#if GO_MPI == 1
	MPI_Init(&argc, &argv);
#endif

#if GO_MPI == 1
	MPI_Comm_rank(MPI_COMM_WORLD, &myrank);
	MPI_Comm_size(MPI_COMM_WORLD, &np);
#endif

	if (myrank == 0) cout << "number of processes = " << np << endl;

	double aclock1, aclock2;

	init_params( a);

	aclock1 = mclock();

	// EVERYDAY - everyday use
	// FILEOUT - full file output
	// TEXTOUT - full comments output
	// MPIESTIMATE - MPI performance estimation
	elastcyl_2d( EVERYDAY );

	//mpi_estimate();

	aclock2 = mclock();
	if (myrank == 0) cout << "Total time: " << diffclock(aclock1, aclock2) << "s" << endl << endl;


#if GO_MPI == 1
	MPI_Barrier(MPI_COMM_WORLD);
	MPI_Finalize();
#endif

	return 0;
}

