from mpl_toolkits.mplot3d import Axes3D
import matplotlib 
matplotlib.use('Agg')
from matplotlib import pyplot as plt
from matplotlib import cm               # colormaps
from matplotlib.mlab import griddata

import numpy as np
#import Tkinter
import sys
import cmath

from pylab import *

global epsilon
epsilon = 0.01


def makeplot_colormesh(X, Y, Z,endofname):

    ax = fig.add_subplot(1, 1, 1)

    gridN = 1000

    xi = np.linspace(-1.1,1.1,gridN)
    yi = np.linspace(-1.1,1.1,gridN)
    xxi, yyi = np.meshgrid(xi, yi)
    zi = griddata(X,Y,Z,xi,yi, interp='linear')
    zzi = np.reshape( zi.filled(0), (gridN,gridN) )
    
    for i in range(0, gridN ):
        for j in range(0, gridN ):
          if( abs(xi[i] + yi[j]*1j) > 1.0 ):
            zzi[i,j] = 0.0
          if( (cmath.phase( complex(xi[i], yi[j]) ) <= cmath.pi/4) and ( cmath.phase(complex(xi[i], yi[j]) ) >= -cmath.pi/4) or abs(xi[i] + yi[j]*1j) < epsilon ):
              zi.mask[i,j] = True

    vmint = -0.2
    vmaxt = 0.4   

    if( endofname == "sigma_e"):
      vmint = 0
      vmaxt = 0.2

    #ax.scatter(X, Y, Z, s=1,marker = ".", c="r", cmap=cm.coolwarm)
    #ax.contourf(xi, yi, zzi, zdir='y', offset=40, cmap=cm.coolwarm)
    cax = ax.pcolormesh(xi, yi, zi, vmin = vmint, vmax = vmaxt );

    plt.xlim(-1.1, 1.1)
    plt.ylim(-1.1, 1.1)

    cbar = fig.colorbar(cax)

    cbar.update_ticks()
    #cbar.ax.set_yticklabels(['< -0.2', '> 0.4'])
    
   # ax.contourf(X, Y, Z, levels,
    #                    cmap=cm.get_cmap(cmap, len(levels)-1),
      #                  norm=norm,
    ##                    )

    stepname = str(nl)
    fname = endofname + stepname
    pltfname = "plotcm_" + fname + ".png"
    plt.savefig(pltfname, dpi = 300)
    print 'Plot saved as %s' % pltfname
    
    fig.delaxes(ax)  
    fig.delaxes(fig.axes[0]) 
  
    plt.cla()

    #plt.show()


def makeplot_dots(X,Y,Z,endofname):

    ax = fig.add_subplot(1, 1, 1, projection='3d')

    #ax.set_title('View', fontsize=20)

    # axes limits
    #
    ax.set_xlim(-1, 1)
    ax.set_ylim(-1, 1)
    ax.set_zlim(-1, 1)
    X = np.concatenate([X,[0,0]])
    Y = np.concatenate([Y,[0,0]])
    Z = np.concatenate([Z,[-1,1]])
    
    ax.scatter(X, Y, Z, s=1,marker = ".", c="r")

    stepname = str(nl)
    fname = endofname + stepname
    pltfname = "plotd_" + fname + ".png"
    plt.savefig(pltfname, dpi = 300)
    print 'Plot saved as %s' % pltfname
    
    fig.delaxes(ax)    



def plotsteps( endofname ):

#if(True):    
  #endofname = "I1"

  shift = 451

  k = 26136
  loading = True

  global fig, ax, nl, kmax, r, theta, urur

  nl = 217
  kmax = 7000000

  fig = plt.figure()

  while loading:

    k = k + 1

    stepnum = k
    stepname = str(stepnum)
    fname = "step" + stepname

    loaded = False

    try:
	infname1 = fname + endofname + ".txt"
	#infname2 = fname + "t.txt"
	#infname3 = fname + "r_mpi.txt"
	r, theta, ur = np.loadtxt(infname1, unpack=True)
	#r, theta, ut = np.loadtxt(infname2, unpack=True)
        #r, theta, ur_mpi = np.loadtxt(infname3, unpack=True)
	#ut = [0, 0]
	#ur_mpi = [0, 0]
	loaded = True
        nl = nl + 1
    except:
	if k == 0:
	    #err_msg = "Could not load data from file %s or %s." % infname1 % infname2 \
            #  + " Did you forget to run the program?"
	    #raise Exception(err_msg)
	    loaded = False
	else:
	    #print "%d steps were loaded" % nl
	    loaded = False
	
    if (loaded == False):
        if (k < kmax):
            continue
        else:
            print "%d steps were loaded" % nl
	    break


    #print u
    #print x

    n = r.size

    #ax.clf()
    
    X = np.zeros(n)
    Y = np.zeros(n)
    Z = np.zeros(n)
    Z = ur

    for i in range(0, n ):
      X[i] = r[i]*cmath.sin(theta[i])
      Y[i] = r[i]*cmath.cos(theta[i]) 

    #ax.set_title('View', fontsize=20)

    # axes limits
    #
    #ax.set_xlim(-1, 1)
    #ax.set_ylim(-1, 1)
    #ax.set_zlim(-0.3, 0.3)
    #X = np.concatenate([X,[0,0]])
    #Y = np.concatenate([Y,[0,0]])
    #Z1 = np.concatenate([Z1,[-1,1]])
    #Z2 = np.concatenate([Z2,[-1,1]])
    #Z3 = np.concatenate([Z3,[-1,1]])

    #makeplot_dots(X,Y,Z,endofname)
    makeplot_colormesh(X,Y,Z,endofname)



def plotstepsCP( endofname ):

#if(True):    
  #endofname = "I1"

  k = 66000
  loading = True

  global fig, ax, nl, kmax, r, theta, urur

  nl = 0
  kmax = 70000

  fig = plt.figure()

  u = []
  u_outer = []

  while loading:

    k = k + 1

    stepnum = k
    stepname = str(stepnum)
    fname = "step" + stepname

    loaded = False

    try:
	infname1 = fname + endofname + ".txt"
	#infname2 = fname + "t.txt"
	#infname3 = fname + "r_mpi.txt"
	r, theta, ur = np.loadtxt(infname1, unpack=True)
	#r, theta, ut = np.loadtxt(infname2, unpack=True)
        #r, theta, ur_mpi = np.loadtxt(infname3, unpack=True)
	#ut = [0, 0]
	#ur_mpi = [0, 0]
	loaded = True
        nl = nl + 1
    except:
	if k == 0:
	    #err_msg = "Could not load data from file %s or %s." % infname1 % infname2 \
            #  + " Did you forget to run the program?"
	    #raise Exception(err_msg)
	    loaded = False
	else:
	    #print "%d steps were loaded" % nl
	    loaded = False
	
    if (loaded == False):
        if (k < kmax):
            continue
        else:
            print "%d steps were loaded" % nl
	    break


    #print u
    #print x

    n = r.size

    N = sqrt(n)

    uline = []

    for i in range(0, int(N) ):
      uline.append( ur[N/2 + i*N] )

    #ax = fig.add_subplot(1, 1, 1)

    #plt.set_ylim( -0.5, 0.5 )
    
    plt.ylim(-0.5, 0.5)
    plt.plot(uline, 'r.-')

    stepname = str(nl)
    fname = endofname + stepname
    pltfname = "centerline_" + fname + ".png"
    plt.savefig(pltfname, dpi = 300)
    print 'Plot saved as %s' % pltfname

    plt.cla()

    #fig.delaxes(ax)



    print N

    u.append( ur[N/2 + 2*N] )
    u_outer.append( ur[-N/2 + N*N] )

  print u
  print u_outer

  #fig.delaxes(ax)    

  #plt.show()

  ax = fig.add_subplot(1, 1, 1)

  #stepname = str(nl)
  fname = endofname
  pltfname = "centerline_" + fname + ".png"
  plt.savefig(pltfname, dpi = 300)
  #print 'Plot saved as %s' % pltfname



#plotsteps("r_mpi")
#plotsteps("t_mpi")
#plotsteps("r")
#plotsteps("t")
#plotsteps("r_dr")
#plotsteps("r_dt")
#plotsteps("sigma_rr")
#plotsteps("sigma_tt")
#plotsteps("I1")
plotsteps("sigma_e")
#plotstepsCP("I1")
#plotstepsCP("sigma_e")

plt.savefig("plots", dpi = 10)
