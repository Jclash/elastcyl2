from mpl_toolkits.mplot3d import Axes3D
import matplotlib 
#matplotlib.use('Agg')
from matplotlib import pyplot as plt
from matplotlib import cm               # colormaps

import numpy as np
#import Tkinter
import sys
import cmath

k = 1

k = k-1
loading = True

global fig, ax, nl, kmax

nl = 0
kmax = 10000

fig = plt.figure()

while loading:

    k = k + 1

    stepnum = k
    stepname = str(stepnum)
    fname = "step" + stepname

    loaded = False

    try:
	infname1 = fname + "r.txt"
	infname2 = fname + "t.txt"
	r, theta, ur = np.loadtxt(infname1, unpack=True)
	r, theta, ut = np.loadtxt(infname2, unpack=True)
	loaded = True
        nl = nl + 1
    except:
	if k == 0:
	    err_msg = "Could not load data from file %s or %s." % infname1 % infname2 \
              + " Did you forget to run the program?"
	    raise Exception(err_msg)
	else:
	    #print "%d steps were loaded" % nl
	    loaded = False
	
    if (loaded == False):
        if (k < kmax):
            continue
        else:
            print "%d steps were loaded" % nl
	    break


    #print u
    #print x

    n = r.size

    #ax.clf()
    ax = fig.add_subplot(1, 1, 1, projection='3d')

    X = np.zeros(n)
    Y = np.zeros(n)
    Z1 = ur
    Z2 = ut

    for i in range(0, n ):
      X[i] = r[i]*cmath.sin(theta[i])
      Y[i] = r[i]*cmath.cos(theta[i]) 
      #if (i == 0):
      #Z1[i] =  0.1

    ax.set_title('View', fontsize=20)
    ax.set_xlim(-1, 1)
    ax.set_ylim(-1, 1)
    ax.set_zlim(-1,1)
    X = np.concatenate([X,[0,0]])
    Y = np.concatenate([Y,[0,0]])
    Z1 = np.concatenate([Z1,[1,-1]])
    Z2 = np.concatenate([Z2,[1,-1]])
    #ax.plot_wireframe(X, Y, Z), 'yo-', rstride=4, cstride=4)
    
    ax.scatter(X, Y, Z1, s=1,marker = ".", c="r")

    plt.show()

    stepname = str(nl)
    fname = "r" + stepname
    pltfname = "plot_" + fname + ".png"
    #plt.savefig(pltfname, dpi = 200)
    #print 'Plot saved as %s' % pltfname
    
    ax.cla()
    ax.scatter(X, Y, Z2, s=1,marker = ".", c="r")

    plt.show()

    stepname = str(nl)
    fname = "t" + stepname
    pltfname = "plot_" + fname + ".png"
    #plt.savefig(pltfname, dpi = 200)
    #print 'Plot saved as %s' % pltfname

    fig.delaxes(ax)    

    

